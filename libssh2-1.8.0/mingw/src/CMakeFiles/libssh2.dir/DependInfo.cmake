# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "RC"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "C:/project/scanner/libssh2-1.8.0/src/agent.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/agent.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/channel.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/channel.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/comp.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/comp.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/crypt.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/crypt.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/global.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/global.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/hostkey.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/hostkey.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/keepalive.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/keepalive.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/kex.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/kex.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/knownhost.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/knownhost.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/mac.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/mac.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/misc.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/misc.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/openssl.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/openssl.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/packet.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/packet.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/pem.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/pem.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/publickey.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/publickey.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/scp.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/scp.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/session.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/session.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/sftp.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/sftp.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/transport.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/transport.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/userauth.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/userauth.c.obj"
  "C:/project/scanner/libssh2-1.8.0/src/version.c" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/version.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "LIBSSH2_CLEAR_MEMORY"
  "LIBSSH2_DH_GEX_NEW=1"
  "LIBSSH2_OPENSSL"
  "LIBSSH2_WIN32"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "C:/project/scanner/OpenSSL-Win32-1.0.2L/include"
  "../include"
  "src"
  )
set(CMAKE_DEPENDS_CHECK_RC
  "C:/project/scanner/libssh2-1.8.0/win32/libssh2.rc" "C:/project/scanner/libssh2-1.8.0/mingw/src/CMakeFiles/libssh2.dir/__/win32/libssh2.rc.obj"
  )

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_RC
  "LIBSSH2_CLEAR_MEMORY"
  "LIBSSH2_DH_GEX_NEW=1"
  "LIBSSH2_OPENSSL"
  "LIBSSH2_WIN32"
  )

# The include file search paths:
set(CMAKE_RC_TARGET_INCLUDE_PATH
  "C:/project/scanner/OpenSSL-Win32-1.0.2L/include"
  "../include"
  "src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
