# CMake generated Testfile for 
# Source directory: C:/project/scanner/libssh2-1.8.0
# Build directory: C:/project/scanner/libssh2-1.8.0/mingw
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("example")
subdirs("tests")
subdirs("docs")
