# CMake generated Testfile for 
# Source directory: C:/project/scanner/libssh2-1.8.0/tests
# Build directory: C:/project/scanner/libssh2-1.8.0/mingw/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_hostkey "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_hostkey.exe")
set_tests_properties(test_hostkey PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_hostkey_hash "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_hostkey_hash.exe")
set_tests_properties(test_hostkey_hash PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_password_auth_succeeds_with_correct_credentials "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_password_auth_succeeds_with_correct_credentials.exe")
set_tests_properties(test_password_auth_succeeds_with_correct_credentials PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_password_auth_fails_with_wrong_password "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_password_auth_fails_with_wrong_password.exe")
set_tests_properties(test_password_auth_fails_with_wrong_password PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_password_auth_fails_with_wrong_username "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_password_auth_fails_with_wrong_username.exe")
set_tests_properties(test_password_auth_fails_with_wrong_username PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_public_key_auth_fails_with_wrong_key "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_public_key_auth_fails_with_wrong_key.exe")
set_tests_properties(test_public_key_auth_fails_with_wrong_key PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_public_key_auth_succeeds_with_correct_rsa_key "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_public_key_auth_succeeds_with_correct_rsa_key.exe")
set_tests_properties(test_public_key_auth_succeeds_with_correct_rsa_key PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_public_key_auth_succeeds_with_correct_dsa_key "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_public_key_auth_succeeds_with_correct_dsa_key.exe")
set_tests_properties(test_public_key_auth_succeeds_with_correct_dsa_key PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_keyboard_interactive_auth_fails_with_wrong_response "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_keyboard_interactive_auth_fails_with_wrong_response.exe")
set_tests_properties(test_keyboard_interactive_auth_fails_with_wrong_response PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
add_test(test_keyboard_interactive_auth_succeeds_with_correct_response "C:/project/scanner/libssh2-1.8.0/mingw/tests/test_keyboard_interactive_auth_succeeds_with_correct_response.exe")
set_tests_properties(test_keyboard_interactive_auth_succeeds_with_correct_response PROPERTIES  WORKING_DIRECTORY "C:/project/scanner/libssh2-1.8.0/tests")
