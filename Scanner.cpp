#include "stdafx.h"
#include "scanner.h"
#include "Connector.h"
#include "ThreadPool.h"

#include <exception>

const DWORD TIME_QUANTUM = 300; //ms
const DWORD ScannerTimeout = 30 * 1000;
const bool TScanner::UseAsyncFTPConnection = false;
DWORD TScanner::ScanStartMark = 0;
TConfig *Config;
exception_ptr ScannerException, ConfigException;

// the main class of the file
TScanner::TScanner(void *ACallbackData)// : Config(ServerList) 
{
  Config = new TConfig();
  ::Config = Config;
  TServer::Scanner = this;
  TLoginDataPointer::Config = Config;
  CallbackData = ACallbackData;
  ThreadPool = new TThreadPool(this);
  Connector = new TConnector(ThreadPool);
  Connector->OnConnectionComplete = &TThreadPool::TaskCompleteCallback;
  OnRequestComplete = NULL;
  OnOutputDataBlock = NULL;
  OnAddressDone = NULL;
  ScanStartMark = GetTickCount();
  FeedThread = NULL;
  StopScan = false;
  InitializeCriticalSection(&ServerListGuard);
}

TScanner::~TScanner()
{
  StopScan = true;
  ThreadPool->StopTasks = true;
  WaitForSingleObject(FeedThread, 3000);
  delete Connector;
  delete ThreadPool;
  delete Config;
  CloseHandle(FeedThread);
  DeleteCriticalSection(&ServerListGuard);
}

DWORD TScanner::ScanTime()
{
  //return value is overflowed each 49 days from start 
  DWORD Result = GetTickCount() - ScanStartMark;
  return Result;
  return 0;
}

void TScanner::ServerConnectComplete(TConnection *Connection)
{
  EnterCriticalSection(&ServerListGuard);
  if(OnRequestComplete) OnRequestComplete(Connection, CallbackData);
  Connection->Server->NextProbeTime = Connection->Server->ProbeInterval + TScanner::ScanTime();
  Connection->Server->NextLoginData();
  Connection->Server->Connecting = false;
  if(Connection->Server->State == TServerState::Unreachable)
    Connection->Server->LoginDataPointer.SetNull();
  if(Connection->Result)
  {
    Connection->Server->State = TServerState::LoginFound;
    Results.Items.push_back(Connection);
  }
  else
    delete Connection;
  LeaveCriticalSection(&ServerListGuard);
  if(Results.Items.size() >= (unsigned int)Config->MaxCallbackDataSize)
  {
    if(OnOutputDataBlock) OnOutputDataBlock(Results.Items, CallbackData);
    Results.Free(); 
  }
}

void TScanner::FeedTasks()
{
  TAddressPointer AddressPointer[TProtocol::Count];
  bool NoTasks = true;
  for(int p = 0; p < TProtocol::Count; p++)
    if(AddressPointer[(TProtocol::Value)p].First((TProtocol::Value)p)) NoTasks = false;
  if(NoTasks) return;
  EnterCriticalSection(&ServerListGuard);
  list<TServer *>::iterator ServerPtr = ServerList.Items.begin();
  while(!StopScan)
  {
    if(ServerPtr == ServerList.Items.end())// || ThreadPool->TaskCount >= Config->MaxConnectionCount)
    { 
      LeaveCriticalSection(&ServerListGuard);
      Sleep(TIME_QUANTUM);
      EnterCriticalSection(&ServerListGuard);
      ServerPtr = ServerList.Items.begin();
    }
    int i = ThreadPool->TaskCount;
    if(ServerPtr == ServerList.Items.end() || ServerList.Items.size() < Config->MaxConnectionCount) //|| ThreadPool->TaskCount < Config->MaxConnectionCount)
      for(int p = 0; p < TProtocol::Count; p++)
      {
        if(!ServerList.Add(AddressPointer[p], (TProtocol::Value)p)) continue;
        if(ServerList.Items.size() == 1) ServerPtr = ServerList.Items.begin();
      }
    if(ServerList.Items.empty()) throw TScannnerException("FeedTasks error: Servers config is empty");
    TServer *Server = *ServerPtr;
    if(Server->Done())
    {
      if(OnServerDone) OnServerDone(Server, CallbackData);
      if(OnAddressDone) OnAddressDone(Server->Address, CallbackData);
      if(Server->State != TServerState::LoginFound) delete *ServerPtr;
      ServerPtr = ServerList.Items.erase(ServerPtr);
      if(ServerList.Items.empty() &&
        AddressPointer[TProtocol::FTP].AtTheEnd() &&
        AddressPointer[TProtocol::SSH].AtTheEnd()) break;
      if(ServerPtr == ServerList.Items.end())
        ServerPtr = ServerList.Items.begin();
      continue;
    }
    ServerPtr++;
    if(Server->Connecting || ScanTime() < Server->NextProbeTime) continue;
    Server->Connecting = true;
    string Login, Password;
    if(!Server->LoginDataPointer.GetLoginData(Server->Address, Login, Password)) continue;
    ThreadPool->AddTask(new TConnection(Server, Login, Password));
  }
  LeaveCriticalSection(&ServerListGuard);
  DWORD w = WaitForSingleObject(ThreadPool->NoRunningTasksFlag, ScannerTimeout);
  if(w == WAIT_TIMEOUT)
    ThreadPool->Terminate();
  if(!Results.Items.empty())
  {
    if(OnOutputDataBlock) OnOutputDataBlock(Results.Items, CallbackData);
    Results.Free();
  }
  ServerList.Free();
}

DWORD WINAPI TScanner::FeedLoginsCallback(CONST LPVOID Param)
{
  ((TScanner *)Param)->FeedTasks();
  return 0;
}

bool TScanner::Scan()
{
  if(FeedThread)
    switch(WaitForSingleObject(FeedThread, 0))
    {
    case WAIT_OBJECT_0:
      CloseHandle(FeedThread);
      break;
    case WAIT_TIMEOUT:
      return false; // still running
    default: throw runtime_error("WaitForSingleObject error");
    }

  FeedThread = CreateThread(NULL, 0, &FeedLoginsCallback, this, 0, NULL);
  if(FeedThread) 
    ScanStartMark = GetTickCount();
  else
    throw std::runtime_error("CreateThread error");
  if(!(ScannerException == NULL)) rethrow_exception(ScannerException);
  if(!(ConfigException == NULL)) rethrow_exception(ConfigException);
  return true;
}

bool TScanner::WaitForScanComplete(DWORD Timeout)
{
  return WaitForSingleObject(FeedThread, Timeout) != WAIT_TIMEOUT; // WaitForMultipleObjects(2, (const HANDLE *)&ScanCompleteEvent, true, Timeout) != WAIT_TIMEOUT;
}

//TServerList
bool TServerList::Add(TAddressPointer &AddressPointer, TProtocol::Value Protocol)
{
  if(AddressPointer.AtTheEnd()) return false;
  TLoginDataPointer LoginDataPointer;
  if(!Config->GetFirstLoginData(&LoginDataPointer, (TProtocol::Value)Protocol)) return false;
  string a = AddressPointer.Address();
  size_t i = a.rfind(':');
  WORD Port = 0;
  if(i != string::npos)
  {
    try
    {
      Port = atoi(a.c_str() + i);
    } catch (...) {
      Port = 0;
    };
  }
  Items.push_back(new TServer(a, (TProtocol::Value)Protocol, Port, Config->ProbeInterval));
  Items.back()->LoginDataPointer = LoginDataPointer;
  if(!AddressPointer.NextIpInRange())
    AddressPointer.NextSibling();
  return true;
}

void TServerList::Free()
{
  while(!Items.empty())
  {
    delete Items.front();
    Items.pop_front();
  }
  Items.clear(); 
}

//TConnections
void TConnections::Free()
{
  while(!Items.empty())
  {
    delete Items.front();
    Items.pop_front();
  }
  Items.clear(); 
}

TScannnerException::TScannnerException(string Text)
  : runtime_error("Scanner error:"), Text(Text)
{
}

const char* TScannnerException::what() const throw()
{
  return Text.c_str();
}
