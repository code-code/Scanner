#include "stdafx.h"
#include "ThreadPool.h"
#include "Scanner.h"

TThreadPool *TTask::ThreadPool = NULL;

TThreadPool::TThreadPool(TScanner *AScanner)
{
  TTask::ThreadPool = this;
  Scanner = AScanner;
  StopTasks = false;
  NoRunningTasksFlag = CreateEvent(NULL, true, true, NULL);
  TaskCount = 0;
  InitializeCriticalSection(&TaskCountGuard);
  InitializeCriticalSection(&ThreadListGuard);
}

TThreadPool::~TThreadPool()
{
  StopTasks = true;
  WaitForSingleObject(NoRunningTasksFlag, 10000);
  cout << "TThreadPool: Abnormal tasks termination";
  CloseHandle(NoRunningTasksFlag);
  DeleteCriticalSection(&TaskCountGuard);
  DeleteCriticalSection(&ThreadListGuard);
}

DWORD CALLBACK TThreadPool::TaskProcCallback(void *Task)
{
  TTask *t = (TTask *)Task;
  TConnectionThread *ConnectionThread = (TConnectionThread *)t;
  t->ThreadPool->TaskProc(ConnectionThread);
  return 0;
}

void TThreadPool::TaskProc(TConnectionThread *ConnectionThread)
{
  SetThreadPriority(GetCurrentThread(), THREAD_BASE_PRIORITY_MIN);
  EnterCriticalSection(&TaskCountGuard);
  TaskCount++;
  ResetEvent(NoRunningTasksFlag);
  LeaveCriticalSection(&TaskCountGuard);
  Scanner->Connector->Connect(ConnectionThread->Connection);
  //TConnectionThread *ct = dynamic_cast<TConnectionThread *>(ConnectionThread);
  EnterCriticalSection(&ThreadListGuard);
  CloseHandle(ConnectionThread->Handle);
  Threads.erase(ConnectionThread->Iterator);
  delete ConnectionThread;
  LeaveCriticalSection(&ThreadListGuard);
}

void TThreadPool::TaskComplete(TConnection *Connection)
{ 
  Scanner->ServerConnectComplete(Connection);
  EnterCriticalSection(&TaskCountGuard);
  TaskCount--;
  if(TaskCount == 0) SetEvent(NoRunningTasksFlag);
  LeaveCriticalSection(&TaskCountGuard);
}

void TThreadPool::TaskCompleteCallback(TConnection *Connection, void *ThreadPool)
{
  ((TThreadPool *)ThreadPool)->TaskComplete(Connection);
}

bool TThreadPool::AddTask(TConnection *AConnection)
{
  if(StopTasks) return false;
  TConnectionThread *c = new TConnectionThread(AConnection);
  HANDLE h = CreateThread(NULL, 0, TaskProcCallback, (void *)c, 0, NULL);
  if(h == NULL)
  {
    delete c;
    return false;
  }
  c->Handle = h;
  EnterCriticalSection(&ThreadListGuard);
  Threads.push_back(c);
  c->Iterator = Threads.end();
  c->Iterator--;
  LeaveCriticalSection(&ThreadListGuard);
  return true;

  //ULONG flags = WT_EXECUTELONGFUNCTION;
  //if(TScanner::UseAsyncFTPConnection && AConnection->Server->Protocol == TProtocol::FTP)
  //  flags = WT_EXECUTEINPERSISTENTTHREAD;
  //if(!QueueUserWorkItem(TaskProcCallback, (void *)AConnection, flags))
  //  throw std::runtime_error("QueueUserWorkItem Failed");
  //return true;
}

void TThreadPool::Terminate()
{
  EnterCriticalSection(&ThreadListGuard);
  while(!Threads.empty())
  {
    TerminateThread(Threads.front()->Handle, -1);
    CloseHandle(Threads.front()->Handle);
    delete(Threads.front());
    Threads.pop_front();
  }
  Threads.clear(); 
  LeaveCriticalSection(&ThreadListGuard);
}

//TConectionThread
TConnectionThread::TConnectionThread(TConnection *Connection)
  : Connection(Connection)
{
}
