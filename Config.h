//TConfig - ��������� XML ���� �� ���������� ��������� ������
#pragma once
#include <vector>
#include "Server.h"
#include "tinyxml/tinyxml.h"

using namespace std;

class TConfig
{
private:
  TiXmlDocument XmlDoc;
  TiXmlElement *ConfigElement;
  TLoginDataPointer InitLoginDataPointer[TProtocol::Count];
  bool AddAddresses(TiXmlElement *Addresses);
  bool AddLoginMacros(TiXmlElement *Logins, int ProtocolIndex);
  bool AddLogins(TiXmlElement *Logins, int ProtocolIndex, string Macro);
public:
  list<string> AddressList[TProtocol::Count];
  int MaxCallbackDataSize;
  int DefaultProbeInterval;
  WORD ProbeInterval;
  int MaxConnectionCount;
  CRITICAL_SECTION ConfigGuard;
  TConfig();
  ~TConfig();
  bool LoadFromXml(TiXmlNode *ParentNode);
  void LoadFromBuf(const char *XmlConfig);
  bool LoadFromFile(string XmlFileName);
  bool GetFirstLoginData(TLoginDataPointer *LoginDataPointer, TProtocol::Value Protocol = TProtocol::None);
  bool GetNextLoginData(TLoginDataPointer &LoginDataPointer);
  TiXmlElement *GetFirstAddress(TProtocol::Value Protocol);
  static bool ExpandDomain(string &Macro, string Domain);
  static UINT IpToUlong(const string &IpString);
  static string UlongToIp(ULONG IpUlong);
};

class TConfigException : public std::runtime_error
{
private:
  string Text;
public:
  TConfigException(string Text);
  const char* what() const throw();
};