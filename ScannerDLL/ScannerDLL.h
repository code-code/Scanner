#pragma once
#include <Windows.h>
#include <winnt.h>
#include <string>

//#include <winnt.h>
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SCANNERDLL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SCANNERDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef SCANNERDLL_EXPORTS
#define SCANNERDLL_API extern "C" __declspec( dllexport )
//#define SCANNERDLL_API __declspec(dllexport)
#else
#define SCANNERDLL_API extern "C" __declspec(dllimport)
//#define SCANNERDLL_API __declspec(dllimport)
#endif

#ifndef LPCBYTE
# define LPCBYTE const char*
#endif
#ifndef CHAR
# define CHAR char
#endif


struct ParentInfo 
{
  CHAR ParentID[256];
  CHAR ParentGroup[64];
  CHAR SelfIP[64];
  LPCWSTR ParentFiles;
};

SCANNERDLL_API PVOID __stdcall Start(
  LPCSTR ModuleName,
  LPCBYTE Arg,
  SIZE_T ArgLen,
  LPSTR ResultInfo,
  const ParentInfo* pParentData,
  PVOID EventCallback,
  PVOID EventCallbackContext,
  PVOID Reserved1);

SCANNERDLL_API BOOL __stdcall Control(
  PVOID ModuleHandle,
  LPCSTR Ctl,
  LPCBYTE CtlArg,
  SIZE_T CtlArgLen,
  LPSTR CtlResultInfo,
  PVOID* ppOutData,
  PDWORD pOutDataSize,
  LPCSTR pOutDataTag,
  PVOID Reserved1);

SCANNERDLL_API VOID __stdcall Release(PVOID ModuleHandle);

SCANNERDLL_API VOID __stdcall FreeBuffer(PVOID pMemory);
