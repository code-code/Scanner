#include "stdafx.h"
#include "ScannerDLL.h"
#include "../Scanner.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

typedef VOID (__stdcall *TEventCallback)(
  PVOID ModuleHandle,
  LPCSTR EventName,
  LPCSTR EventInfo,
  PVOID pOutData,
  DWORD OutDataSize,
  LPCSTR pOutDataTag,
  PVOID Context);

typedef void (CALLBACK *TStatisticsEvent)(const char *Data);

struct TScannerWrapper
{
  TScanner *Scanner;
  TEventCallback EventCallback;
  TStatisticsEvent OnStatistics;
  struct
  {
    int LoginCount;
    int ServerDoneCount;
    DWORD ServerDoneMark;
    DWORD ConnectionCountMark;
    static const int UpdateInterval = 1000;
  } Stat;
};

// note: the callback is called by multiple threads
void CALLBACK OutputDataBlock(list<TConnection *> &Results, void *CallbackData)
{
  TScannerWrapper *w = (TScannerWrapper *)CallbackData;
  string s;
  for(list<TConnection *>::iterator i = Results.begin(); i != Results.end(); i++)
  {
    TConnection *Connection = *i;
    s.append(Connection->Server->Address + "," +
      Connection->Login() + "," + Connection->Password() + "\r\n"); 
  }
  w->EventCallback(NULL, NULL, NULL, (PVOID)s.c_str(), (DWORD)s.length(), NULL, NULL);
}

void CALLBACK ConnectionRequestComplete(TConnection *Connection, void *CallbackData)
{
  TScannerWrapper *w = (TScannerWrapper *)CallbackData;
  if(Connection->Server->State != TServerState::Unavailable) 
  {
    w->Stat.LoginCount++;
    if(w->OnStatistics && GetTickCount() > w->Stat.ConnectionCountMark + w->Stat.UpdateInterval)
    {
      stringstream ss;
      ss << "Logins checked: " << w->Stat.LoginCount;
      w->OnStatistics(ss.str().c_str());
      w->Stat.ConnectionCountMark = GetTickCount();
    }
  }
}

void CALLBACK ServerDone(string Address, void *CallbackData)
{
  TScannerWrapper *w = (TScannerWrapper *)CallbackData;
  w->Stat.ServerDoneCount++;
  if(w->OnStatistics && GetTickCount() > w->Stat.ServerDoneMark + 1000)
  {
    stringstream ss;
    ss << "Servers done: " << w->Stat.ServerDoneCount << ", " << Address;
    w->OnStatistics(ss.str().c_str());
    w->Stat.ServerDoneMark = GetTickCount();
  }
}

SCANNERDLL_API PVOID __stdcall Start(LPCSTR ModuleName, LPCBYTE Arg, SIZE_T ArgLen, LPSTR ResultInfo, const ParentInfo* pParentData, PVOID EventCallback, PVOID EventCallbackContext, PVOID Reserved1)
{
  TScannerWrapper *w = new TScannerWrapper;
  w->EventCallback = (TEventCallback)EventCallback;
  try
  {
    w->Scanner = new TScanner((void *)w);
  } catch(const runtime_error& e) 
  {
    OutputDebugStringA(string(string("Error while creating TScanner: ") + e.what()).c_str());
  }
  catch (...)
  {
    OutputDebugStringA("Unknown error while creating TScanner");
  }
  w->Stat.LoginCount = 0;
  w->Stat.ServerDoneCount = 0;
  w->Scanner->OnOutputDataBlock = OutputDataBlock;
  w->Scanner->OnRequestComplete = (TConnectionRequestCompleteEvent)ConnectionRequestComplete;
  w->Scanner->OnServerDone = (TServerDoneEvent)ServerDone;
  w->OnStatistics = (TStatisticsEvent)Reserved1;
  return w;
}

SCANNERDLL_API BOOL __stdcall Control(PVOID ModuleHandle, LPCSTR Ctl, LPCBYTE CtlArg, SIZE_T CtlArgLen, LPSTR CtlResultInfo, PVOID* ppOutData, PDWORD pOutDataSize, LPCSTR pOutDataTag, PVOID Reserved1)
{
  bool Result = false;
  TScannerWrapper *w = (TScannerWrapper *)ModuleHandle;
  if(string(Ctl) == "SetConf")
  {
    try
    { 
      w->Scanner->Config->LoadFromBuf((const char *)CtlArg);
      *ppOutData = _strdup("Xml config successfully loaded");
      *pOutDataSize = (DWORD)strlen((const char *)*ppOutData);
      Result = true;
    }
    catch(const runtime_error& e)
    {
      OutputDebugStringA(string(string("TScanner.Config.LoadFromBuf error: ") + e.what()).c_str());
    }
    catch (...)
    {
      OutputDebugStringA("Unknown TScanner.Config.LoadFromBuf error");
    }
  }
  else if(string(Ctl) == "StartScan")
  {
    try
    {
      w->Stat.ConnectionCountMark = GetTickCount();
      w->Stat.ServerDoneMark = GetTickCount();
      w->Scanner->Scan();
      *ppOutData = _strdup("Scanning started...");
      *pOutDataSize = (DWORD)strlen((const char *)*ppOutData);
      Result = true;
    }
    catch(const runtime_error& e)
    {
      OutputDebugStringA(string(string("TScanner.Scan error: ") + e.what()).c_str());
    }
    catch (...)
    {
      OutputDebugStringA("Unknown TScanner.Scan error");
    }
  }
  return Result;
}

SCANNERDLL_API VOID __stdcall FreeBuffer(PVOID pMemory)
{
  free(pMemory);
}

SCANNERDLL_API VOID __stdcall Release(PVOID ModuleHandle)
{
  TScannerWrapper *w = (TScannerWrapper *)ModuleHandle;
  delete w->Scanner;
  delete w;
}
