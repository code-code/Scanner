#include "stdafx.h"
#include "ScannerDLL.h"

using namespace std;
//using namespace ScannerNamespace;

CRITICAL_SECTION OutBlockGuard;
DWORD OutDataMark;

void __stdcall DataBlockComplete(PVOID ModuleHandle,
  LPCSTR EventName,
  LPCSTR EventInfo,
  PVOID pOutData,
  DWORD OutDataSize,
  LPCSTR pOutDataTag,
  PVOID Context)
{
  EnterCriticalSection(&OutBlockGuard);
  //cout << (char *)pOutData << "\r\n";
  ofstream f("OutData.txt", std::ios_base::app);
  if(f)
    f << (char *)pOutData << "\r\n\r\n";
  else
    cout << "Error: cannot open file\r\n";
  LeaveCriticalSection(&OutBlockGuard);
}

void CALLBACK PrintStatistics(char *Data)
{
  EnterCriticalSection(&OutBlockGuard);
  cout << Data << "\r\n";
  LeaveCriticalSection(&OutBlockGuard);
}


void Scan(PVOID Scanner, string Filename)
{
  ifstream stream;
  stream.open(Filename);
  string Xml((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
  PVOID OutData;
  DWORD OutDataSize;
  string CtlArg = "SetConf";
  if(Control(Scanner, CtlArg.c_str(), Xml.c_str(), Xml.length(), NULL, &OutData, &OutDataSize, NULL, NULL))
  {
    cout << CtlArg.c_str() << " response: " << (char *)OutData << "\r\n";
    FreeBuffer(OutData);
  } else
    throw std::runtime_error(CtlArg + " error\r\n");
    //cout << CtlArg.c_str() << " error\r\n";
  CtlArg = "StartScan";
  InitializeCriticalSection(&OutBlockGuard);
  if(Control(Scanner, CtlArg.c_str(), Xml.c_str(), Xml.length(), NULL, &OutData, &OutDataSize, NULL, NULL))
  {
    OutDataMark = GetTickCount();
    cout << CtlArg.c_str() << " response: " << (char *)OutData << "\r\n";
    FreeBuffer(OutData);
  } else
    throw std::runtime_error(CtlArg + " error\r\n");
    //cout << CtlArg.c_str() << " error\r\n";
}

int main(int argc, _TCHAR* argv[])
{
  if(argc != 2)
  {
      cout << "Usage: DLLTest <Filename>\r\n";
      _getch();
      return -1;
  }
  PVOID Scanner = Start(NULL, NULL, 0, NULL, NULL, &DataBlockComplete, (PVOID)1, &PrintStatistics);
  if(!Scanner)
  {
    cout << "Cannot create scanner using the Start command\r\n";
    _getch();
    return -1;
  }
  try
  {
    string f(argv[1]);
    Scan(Scanner, argv[1]);
    //Scan(Scanner, "..\\config.xml");
  }
  catch (const std::runtime_error& e)
  {
  	cout << "Memory exception: " << e.what();
  }
  catch (const std::exception& e)
  {
    cout << "Exception: " << e.what();
  }  
  _getch();
  Release(Scanner);
	return 0;
}

