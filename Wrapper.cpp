#include "StdAfx.h"
#include "Wrapper.h"
#include "Scanner.h"

BOOL APIENTRY DllMain( HANDLE hModule,
                      DWORD  ul_reason_for_call,
                      LPVOID lpReserved)
{
  switch (ul_reason_for_call)
  {
  case DLL_PROCESS_ATTACH:
  case DLL_THREAD_ATTACH:
  case DLL_THREAD_DETACH:
  case DLL_PROCESS_DETACH:
    break;
  }
  return TRUE;
}

SCANNER_API int nScanner=0;

// C-style wrapper for dll
SCANNER_API PVOID __stdcall Start(LPCSTR ModuleName, LPCBYTE Arg, SIZE_T ArgLen, LPSTR ResultInfo, const ParentInfo* pParentData, PVOID EventCallback, PVOID EventCallbackContext, PVOID Reserved1)
{
  return new TScanner(EventCallbackContext);
}

SCANNER_API BOOL __stdcall Control(PVOID ModuleHandle, LPCSTR Ctl, LPCBYTE CtlArg, SIZE_T CtlArgLen, LPSTR CtlResultInfo, PVOID* ppOutData, PDWORD pOutDataSize, LPCSTR pOutDataTag, PVOID Reserved1)
{
  return true;
}

SCANNER_API VOID __stdcall Release(PVOID ModuleHandle)
{
  delete (TScanner *)ModuleHandle; 
}

SCANNER_API VOID __stdcall FreeBuffer(PVOID pMemory)
{

}