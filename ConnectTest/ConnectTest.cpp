// FtpTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConnectTest.h"
#include "../Connector.h"
#include "../../BruteBot/Utils/Utils.h";

//
//int readServ(int s) 
//{
//  int rc;
//  fd_set fdr;
//  FD_ZERO(&fdr);
//  FD_SET(s, &fdr);
//  timeval timeout;
//  timeout.tv_sec = 2;
//  timeout.tv_usec = 0;
//  do 
//  {
//    char buff[512] = {0};
//    recv(s, buff, 512, 0);
//    cout << buff;
//    rc = select(s + 1, &fdr, NULL, NULL, &timeout);
//  } while (rc);
//  return 2;
//}


void CALLBACK ConnectionComplete(TConnection *Connection, void *CallbackData)
{
  cout << "Address=" << Connection->Server->Address
    << ", Login=" << Connection->Login()
    << ", Password=" << Connection->Password()
    << ", Result=" << Connection->Result
    << endl;
  if(Connection->Server->State == TServerState::Unreachable)
    cout << "Server is unreachable" << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
  WSADATA wsaData;
  int iResult;
  iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
  if (iResult != 0) 
  {
    printf("WSAStartup failed: %d\n", iResult);
    return 1;
  }
  ifstream is("..\\..\\ConnectTest.txt");
  if(!is) return -1;
  string s;
  is >> s;
  is.close();
  TStringList ss(s, ',');
  vector<string> l(ss.Items.begin(), ss.Items.end());
  while(true)
  {
    list<string>::iterator i = ss.Items.begin();
    TServer s(l[0], TProtocol::SSH);
    i++;
    TConnection c(&s, l[1], l[2]);
    c.Connector = new TConnector();
    c.Connector->OnConnectionComplete = &ConnectionComplete;
    cout << "Connecting..." << endl;
    c.Connector->Connect(&c);
    Sleep(2000);
  }

  getch();
  return 0;
}

