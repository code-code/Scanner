//TServer - ������ ���������� ��� ����������� � �������
#pragma once
#include <string>
#include <list>
#include "tinyxml/tinyxml.h"

using namespace std;
//IP or domain address
typedef string TAdress;
//Internet protocol
struct TProtocol;
struct TProtocol
{
  enum Value {FTP = 0, SSH, Count, None};
  static const char *String[TProtocol::Count];
};

class TConfig;
class TLoginDataPointer
{
public:
  static TConfig *Config;
  TiXmlElement *LoginList;
  TiXmlElement *LoginMacro;
  TiXmlElement *Login;
  TiXmlElement *PasswordMacro;
  TiXmlElement *Password;
  TLoginDataPointer();
  bool GetLoginData(string Domain, string &ALogin, string &APassword);
  void SetNull();
};
//handles DNS, ip addresses and ip ranges
class TAddressPointer
{
private:
   bool LoadIpRange();
public:
  TiXmlElement *AddressElement;
  ULONG CurrentIp;
  ULONG EndIp;
  bool HasIpRange;
  string Address();
  bool First(TProtocol::Value Protocol);
  bool NextIpInRange();
  bool NextSibling();
  bool AtTheEnd();
};

struct TServerState
{
  enum Value {NotChecked, Unreachable, Available, LoginFound};
};

class TScanner;
//Describes server with its address, logins, and some data for scanning 
class TServer
{
private:
public:
  static TScanner *Scanner;
  TAdress Address;
  UINT16 Port;
  TProtocol::Value Protocol;
  TLoginDataPointer LoginDataPointer;
  WORD ProbeInterval;// ms
  DWORD NextProbeTime;// ms
  bool Connecting;
  TServerState::Value State;
  TServer(TAdress AAdress, TProtocol::Value AProtocol, WORD APort = 0, WORD AProbeInterval = 0);
  ~TServer();
  bool NextLoginData();
  bool Done();
};
