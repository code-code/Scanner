#pragma once
#include <vector>
#include "Connector.h"

class TScanner;
class TThread;
class TTask;
class TConnectionThread;
class TThreadPool
{
private:
  CRITICAL_SECTION TaskCountGuard;
  CRITICAL_SECTION ThreadListGuard;
  static DWORD CALLBACK TaskProcCallback(void *Task);
  void TaskProc(TConnectionThread *ConnectionThread);
public:
  TScanner *Scanner;
  volatile LONG TaskCount;
  HANDLE NoRunningTasksFlag;
  bool StopTasks;
  list<TConnectionThread *> Threads; 
  TThreadPool(TScanner *AScanner);
  ~TThreadPool();
  static void CALLBACK TaskCompleteCallback(TConnection *Connection, void *ThreadPool);
  void TaskComplete(TConnection *Connection);
  bool AddTask(TConnection *AConnection);
  void Terminate();
};

class TConnectionThread
{
public:
  HANDLE Handle;
  TConnection *Connection;
  list<TConnectionThread *>::iterator Iterator;
  TConnectionThread(TConnection *Connection);
};

class TTask : public TConnectionThread
{
public:
  static TThreadPool *ThreadPool;
};



