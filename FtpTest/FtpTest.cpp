// FtpTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "FtpTest.h"


void communication(int sock, const char *msg)
{
  string response;
  char server_reply[2000];
  printf("Msg: %s\n", msg);
  send(sock, msg, strlen(msg), 0);
  //Receive a reply from the server
  int rval;
  do
  {
    rval = (int) recv(sock , server_reply , 2000 , 0);
    printf("Received bytes: %d\n", rval);
    if (rval <= 0)
      break;
    else 
    {
      response.append(server_reply);
      puts(response.c_str());
    }
  }
  while (true);
}




ULONG GetIP(const string Address)
{
  char c = Address[Address.length() - 1];
  if(c >= '0' && c <= '9')
    return inet_addr(Address.c_str());
  else
  {
    HOSTENT *h = gethostbyname(Address.c_str()); 
    return ((in_addr*)h->h_addr_list[0])->s_addr;
  }
  return 0;
}

int readServ(int s) 
{
  int rc;
  fd_set fdr;
  FD_ZERO(&fdr);
  FD_SET(s, &fdr);
  timeval timeout;
  timeout.tv_sec = 2;
  timeout.tv_usec = 0;
  do 
  {
    char buff[512] = {0};
    recv(s, buff, 512, 0);
    cout << buff;
    rc = select(s + 1, &fdr, NULL, NULL, &timeout);
  } while (rc);
  return 2;
}

int FindFtpResponse(char *Response, char *Code, int CodeLen)
{
  char *BufPtr = Response;
  int Result = -1;
  while(*BufPtr)
  {
    if(strncmp(BufPtr, Code, CodeLen) == 0) Result = 0;
    BufPtr++;
    if(strncmp(BufPtr, "\r\n", 2) == 0)
    {
      BufPtr += 2;
      if(BufPtr != 0) return (int)(BufPtr - Response);
    }
  }
  return Result;
}

bool ConnectSocket(SOCKET client, sockaddr_in AddrIn, UINT TimeoutSec)
{
  ULONG NonBlk = 1;
  ULONG Blk = 0;
  int Ret;
  DWORD Err;

  // Open socket
  //client = socket(AF_INET, SOCK_STREAM, 0);

  // Set to Non-blocking mode
  ioctlsocket(client, FIONBIO, &NonBlk);

  // This call will return immediately, coz our socket is non-blocking
  Ret = connect(client, (const sockaddr*)&AddrIn, sizeof(AddrIn));

  // If connected, it will return 0, or error
  if (Ret == SOCKET_ERROR)
  {
    Err = WSAGetLastError();
    // Check if the error was WSAEWOULDBLOCK, where we'll wait.
    if (Err == WSAEWOULDBLOCK)
    {
      //printf("\nConnect() returned WSAEWOULDBLOCK. Need to Wait..");
      fd_set         Write, Err;
      TIMEVAL      Timeout;

      FD_ZERO(&Write);
      FD_ZERO(&Err);
      FD_SET(client, &Write);
      FD_SET(client, &Err);

      Timeout.tv_sec  = TimeoutSec;
      Timeout.tv_usec = 0; // your timeout

      Ret = select (0,                // ignored
        NULL,           // read,
        &Write,        // Write Check
        &Err,            // Error check
        &Timeout);

      if(Ret == 0)
      {
        //printf("\nConnect Timeout (%d Sec).", TimeoutSec);
        return false;
      }
      else
      {
        if (FD_ISSET(client, &Write))
        {
          ioctlsocket(client, FIONBIO, &Blk);
          return true;
        }
        if (FD_ISSET(client, &Err))
          return false;
      }
    }
    else
      return false;
  }
  else
  {
    ioctlsocket(client, FIONBIO, &Blk);
    return true;
  }
  return false;
}

class TScopedSocket
{
public:
  int Socket;
  ~TScopedSocket() {closesocket(Socket);}
};

void ConnectFTPBlockingSock(TConnection *Connection)
{
  ULONG Ip = GetIP(Connection->Server->Address);
  if(!Ip)
  {
    Connection->Server->State = TServerState::Unreachable;
    return;
  }
  if(Connection->Server->Port == INTERNET_INVALID_PORT_NUMBER)
    Connection->Server->Port = INTERNET_DEFAULT_FTP_PORT;
  sockaddr_in AddrIn;

  AddrIn.sin_family = AF_INET;
  AddrIn.sin_addr.s_addr = Ip;
  AddrIn.sin_port = htons(Connection->Server->Port);
  TScopedSocket Socket;
  Socket.Socket = socket(AF_INET, SOCK_STREAM, 0);

  if(!ConnectSocket(Socket.Socket, AddrIn, 10))
  {
    Connection->Server->State = TServerState::Unreachable;
    return;
  }
  char Buf[513] = {0};
  recv(Socket.Socket, Buf, sizeof(Buf) - 1, 0);
  if(strncmp(Buf, "220", 3) != 0) return;
  string Command = "USER " + Connection->Login() + "\r\n";
  send(Socket.Socket, Command.c_str(), Command.length(), 0);
  ZeroMemory(Buf, sizeof(Buf) - 1);
  int Len = recv(Socket.Socket, Buf, sizeof(Buf) - 1, 0);
  if(strncmp(Buf, "331", 3) != 0) return;  
  ZeroMemory(Buf, sizeof(Buf) - 1);
  Command = "PASS " + Connection->Password() + "\r\n";
  send(Socket.Socket, Command.c_str(), Command.length(), 0);
  ZeroMemory(Buf, sizeof(Buf) - 1);
  recv(Socket.Socket, Buf, sizeof(Buf), 0);
  //if(FindFtpResponse(Buf, "230", 3) == -1) return;
  if(strncmp(Buf, "230", 3) != 0) return; 
  Connection->Result = true;
  return;
}

int _tmain(int argc, _TCHAR* argv[])
{
  WSADATA wsaData;
  int iResult;
  iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
  if (iResult != 0) {
    printf("WSAStartup failed: %d\n", iResult);
    return 1;
  }

  TServer s;
  s.Address = "speedtest.tele2.net";
  s.Port = 0;
  TConnection c(&s, "ftp", "p@$$word");
  ConnectFTPBlockingSock(&c);
  getch();
	return 0;
}

