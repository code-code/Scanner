#include <string>

using namespace std;

typedef string TAddress;

struct TServerState
{
  enum Value {NotChecked, Unreachable, Available, LoginFound};
};

class TServer
{
private:
public:
  TAddress Address;
  INTERNET_PORT Port;
  TServerState::Value State;
};

class TConnection
{
private:
  string FLogin;
  string FPassword;
public:
  TServer *Server;
  bool Result;
  TConnection(TServer *AServer, string ALogin, string APassword)
  {
    Server = AServer;
    FLogin = ALogin;
    FPassword = APassword;
  }
  string Login() { return FLogin; };
  string Password() { return FPassword; };
};