#include "StdAfx.h"
#include "Server.h"
#include <intsafe.h>
#include "Scanner.h"
#include "Config.h"

const char *TProtocol::String[] = {"ftp", "ssh"};
TScanner *TServer::Scanner;
TConfig *TLoginDataPointer::Config;

TServer::~TServer()
{
}

TServer::TServer(TAdress AAdress, TProtocol::Value AProtocol, WORD APort /*= 0*/, WORD AProbeInterval /*= 0*/)
{
  Port = APort;
  Connecting = false;
  NextProbeTime = 0;
  Address = AAdress;
  Protocol = AProtocol;
  ProbeInterval = AProbeInterval;
  State = TServerState::NotChecked;
}

bool TServer::NextLoginData()
{
  return Scanner->Config->GetNextLoginData(LoginDataPointer);
}

bool TServer::Done()
{
  return !Connecting &&
    (this->State == TServerState::Unreachable ||
    !LoginDataPointer.LoginList &&
    !LoginDataPointer.LoginMacro && !LoginDataPointer.Login && 
    !LoginDataPointer.PasswordMacro && !LoginDataPointer.Password);
}

//TLoginDataPointer
TLoginDataPointer::TLoginDataPointer()
{
  LoginMacro = 0;
  PasswordMacro = 0;
}

//TLoginDataPointer
bool TLoginDataPointer::GetLoginData(string Domain, string &ALogin, string &APassword)
{
  if(!Login && !Password) return false;
  if(Login->FirstChild())
    ALogin = Login->FirstChild()->ValueStr();
  else
    ALogin = "";
  TConfig::ExpandDomain(ALogin, Domain);
  string Macro;
  if(LoginMacro)
  {
    Macro = LoginMacro->FirstChild()->ValueStr();
    if(Macro != "$original")
      if(TConfig::ExpandDomain(Macro, Domain))
        ALogin = ALogin + Macro;
  }
  if(Password->FirstChild())
    APassword = Password->FirstChild()->ValueStr();
  else
    APassword = "";
  TConfig::ExpandDomain(APassword, Domain);
  if(PasswordMacro)
  {
    Macro = PasswordMacro->FirstChild()->ValueStr();
    if(Macro != "$original")
      if(TConfig::ExpandDomain(Macro, Domain))
        APassword = APassword + Macro;
  }
  return true;
}

void TLoginDataPointer::SetNull()
{
  LoginList = NULL;
  LoginMacro = NULL;
  Login = NULL;
  PasswordMacro = NULL;
  Password = NULL;
}

bool TAddressPointer::LoadIpRange()
{
  const char *RangeDelim = " - ";
  HasIpRange = false;
  CurrentIp = 0;
  EndIp = 0;
  string Addr = AddressElement->FirstChild()->ValueStr();
  size_t RangeDelimPos = Addr.find(RangeDelim);
  if(RangeDelimPos == string::npos)
    return false;
  else
  {
    string s = Addr.substr(0, RangeDelimPos);
    CurrentIp = TConfig::IpToUlong(s);
    s = Addr.substr(RangeDelimPos + strlen(RangeDelim));
    EndIp = TConfig::IpToUlong(s);
  }
  HasIpRange = true;
  return true;
}

std::string TAddressPointer::Address()
{
  if(!AddressElement) return "";
  if(HasIpRange)
    return TConfig::UlongToIp(CurrentIp);
  else
    return AddressElement->FirstChild()->ValueStr();
}

bool TAddressPointer::First(TProtocol::Value Protocol)
{
  AddressElement = Config->GetFirstAddress((TProtocol::Value)Protocol);
  if(AddressElement) LoadIpRange();
  return AddressElement != 0;
}

bool TAddressPointer::NextSibling()
{
  if(!AddressElement) return false;
  AddressElement = AddressElement->NextSiblingElement();
  if(AddressElement) LoadIpRange();
  return AddressElement != NULL;
}

bool TAddressPointer::NextIpInRange()
{
  if(!AddressElement || !HasIpRange) return false;
  if(CurrentIp < EndIp)
  {
    CurrentIp++;
    return true;
  } 
  else
    return false;
  return AddressElement != NULL;
}

bool TAddressPointer::AtTheEnd()
{
  return AddressElement == NULL;
}
