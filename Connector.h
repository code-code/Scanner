#pragma once
#include "Server.h"
#include <WinSock.h>
//#ifdef __MINGW32__
//#include <specstrings.h>
//#endif // __MINGW32__

class TLoginDataPointer;
class TConnector;
class TThreadPool;
class TConnection
{
private:
  string FLogin;
  string FPassword;
public:
  static TConnector *Connector;
  TServer *Server;
  bool Result;
  TConnection(TServer *AServer, string ALogin, string APassword);
  string Login();
  string Password();
};

typedef void (CALLBACK *TConnectionRequestCompleteEvent)(TConnection *Connection, void *CallackData);

class TScanner;
class TConnector
{
private:
  //CRITICAL_SECTION SshHandshakeGuard;
  HINTERNET InternetHandle;//ftp connections
  static void CALLBACK FTPAsyncCallback(__in HINTERNET hInternet, __in DWORD_PTR dwContext, __in DWORD dwInternetStatus,
    __in_bcount(dwStatusInformationLength) LPVOID lpvStatusInformation, DWORD dwStatusInformationLength);
  void ConnectFTPAsync(TConnection *Connection);
  void ConnectFTPBlocking(TConnection *Connection);
  void ConnectFTPBlockingSock(TConnection *Connection);
  void ConnectSSHBlocking(TConnection *Connection);
public:
  //TThreadPool *ThreadPool;
  void *CallbackData;
  TConnectionRequestCompleteEvent OnConnectionComplete;
  TConnector(void *CallbackData = NULL);//TThreadPool *AThreadPool);
  ~TConnector(void);
  void Connect(TConnection *Connection);
};


//ConnectExPtr(s, name, namelen, lpSendBuffer,
//  dwSendDataLength, lpdwBytesSent, lpOverlapped);
//typedef void (*LPFN_CONNECTEX)();
//#define WSAID_CONNECTEX {0x25a207b9, 0xddf3, 0x4660, {0x8e, 0xe9, 0x76, 0xe5, 0x8c, 0x74, 0x06, 0x3e}}
//#define WSAID_DISCONNECTEX {0x7fda2e11, 0x8630, 0x436f, {0xa0, 0x31, 0xf5, 0x36, 0xa6, 0xee, 0xc1, 0x57}}
//
//#ifndef SO_UPDATE_CONNECT_CONTEXT
//# define SO_UPDATE_CONNECT_CONTEXT 0x7010
//#endif
//
//typedef BOOL (PASCAL *LPFN_CONNECTEX)
//  (SOCKET s,
//  const struct sockaddr* name,
//  int namelen,
//  PVOID lpSendBuffer,
//  DWORD dwSendDataLength,
//  LPDWORD lpdwBytesSent,
//  LPOVERLAPPED lpOverlapped);
//
//typedef BOOL (PASCAL *LPFN_DISCONNECTEX)
//  (SOCKET hSocket,
//  LPOVERLAPPED lpOverlapped,
//  DWORD dwFlags,
//  DWORD reserved);
  //WSAOVERLAPPED Overlapped; // ssh socket connections
  //LPFN_CONNECTEX ConnectEx;
  //bool ConnectExInit(SOCKET Socket);
