//������� ��� ������ TScanner � ����� C ��� ��������������� � DLL.
#pragma once
#ifdef SCANNER_EXPORTS
#define SCANNER_API __declspec(dllexport)
#else
#define SCANNER_API __declspec(dllimport)
#endif

typedef const BYTE* LPCBYTE;

typedef struct {
  CHAR ParentID[256];
  CHAR ParentGroup[64];
  CHAR SelfIP[64];
  LPCWSTR ParentFiles;
} ParentInfo;

SCANNER_API PVOID __stdcall Start(
  LPCSTR ModuleName,
  LPCBYTE Arg,
  SIZE_T ArgLen,
  LPSTR ResultInfo,
  const ParentInfo* pParentData,
  PVOID EventCallback,
  PVOID EventCallbackContext,
  PVOID Reserved1);

SCANNER_API BOOL __stdcall Control(
  PVOID ModuleHandle,
  LPCSTR Ctl,
  LPCBYTE CtlArg,
  SIZE_T CtlArgLen,
  LPSTR CtlResultInfo,
  PVOID* ppOutData,
  PDWORD pOutDataSize,
  LPCSTR pOutDataTag,
  PVOID Reserved1);

SCANNER_API VOID __stdcall Release(
  PVOID ModuleHandle);

SCANNER_API VOID __stdcall FreeBuffer(
  PVOID pMemory);

VOID __stdcall EventCallback (
  PVOID ModuleHandle,
  LPCSTR EventName,
  LPCSTR EventInfo,
  PVOID pOutData,
  DWORD OutDataSize,
  LPCSTR pOutDataTag,
  PVOID Context);