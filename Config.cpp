#include "stdafx.h"
#include "Config.h"
//#include <xutility>
//#include <ws2ipdef.h>
//#include <ws2tcpip.h>

namespace TElement
{
#define Enum const char*
  Enum Config = "config";
  Enum MaxConnectionCount = "MaxConnectionCount";
  Enum ProbeInterval = "ProbeInterval";
  Enum MaxCallbackDataSize = "MaxCallbackDataSize";
  Enum AddressList = "addresses";
  Enum LoginList = "logins";
  Enum PasswordList = "passwords";
  Enum MacroList = "macros";
  Enum Protocol = "protocol";
  Enum Address = "a";
  Enum Login = "l";
  Enum Password = "p";
  Enum Macro = "m";
  Enum StartIp = "s";
  Enum EndIp = "e";
#undef Enum
}

TConfig::TConfig()
{
  InitializeCriticalSection(&ConfigGuard);
  for(int i = 0; i < TProtocol::Count; i++)
    ZeroMemory(&InitLoginDataPointer[i], sizeof(TLoginDataPointer));
}

TConfig::~TConfig()
{
}

void TConfig::LoadFromBuf(const char *XmlConfig)
{
  if(!XmlDoc.Parse(XmlConfig)) throw std::runtime_error("TiXmlDocument parse error");
  LoadFromXml((TiXmlNode *)&XmlDoc);
}

bool TConfig::LoadFromFile(string FileName)
{
  if(!XmlDoc.LoadFile(FileName.c_str())) return false;
  LoadFromXml((TiXmlNode *)&XmlDoc);
  return true;
}

bool TConfig::LoadFromXml(TiXmlNode *ParentNode)
{
  if(!ParentNode) return false;
  ConfigElement = ParentNode->FirstChildElement(TElement::Config);
  if(!ConfigElement) return false;
  int i;
  if(!ConfigElement->Attribute(TElement::MaxConnectionCount, &MaxConnectionCount) ||
    !ConfigElement->Attribute(TElement::ProbeInterval, &i) ||
    !ConfigElement->Attribute(TElement::MaxCallbackDataSize, &MaxCallbackDataSize))
    return false;
  if(i <= 65536)
    ProbeInterval = i;
  else
    range_error("ProbeInterval must be <= sizeof(WORD)");
  return true;
}

bool TConfig::GetFirstLoginData(TLoginDataPointer *LoginDataPointer, TProtocol::Value Protocol /*= TProtocol::None*/)
{
  if(Protocol != TProtocol::None)
  {
    if(InitLoginDataPointer[Protocol].LoginList)
    {
      memcpy(LoginDataPointer, &InitLoginDataPointer[Protocol], sizeof(TLoginDataPointer));
      return true;
    }
    TiXmlElement *Addresses = 0;
    for(Addresses = ConfigElement->FirstChildElement(TElement::AddressList);
      Addresses; Addresses = Addresses->NextSiblingElement(TElement::AddressList))
      if(string(Addresses->Attribute(TElement::Protocol)) == TProtocol::String[Protocol])
        break;
    if(!Addresses) return false;
    LoginDataPointer->Login = 0;
    TiXmlElement *LoginList = 0;
    for(LoginList = Addresses->FirstChildElement(TElement::LoginList);
      LoginList; LoginList = LoginList->NextSiblingElement(TElement::LoginList))
    {
      LoginDataPointer->Login = LoginList->FirstChildElement(TElement::Login);
      if(LoginDataPointer->Login) break;
    }
    if(!LoginDataPointer->Login) return false;
    LoginDataPointer->LoginList = LoginList;
  }
  if(!LoginDataPointer->LoginList) return false;
  if(Protocol = TProtocol::None) 
    LoginDataPointer->Login = LoginDataPointer->LoginList->FirstChildElement(TElement::Login);
  TiXmlElement *MacroList = LoginDataPointer->LoginList->FirstChildElement(TElement::MacroList);
  if(MacroList) LoginDataPointer->LoginMacro = MacroList->FirstChildElement(TElement::Macro);
  TiXmlElement *Passwords = LoginDataPointer->LoginList->FirstChildElement(TElement::PasswordList);
  if(!Passwords) return false;
  LoginDataPointer->Password = Passwords->FirstChildElement(TElement::Password);
  if(!LoginDataPointer->Password) return false;
  MacroList = Passwords->FirstChildElement(TElement::MacroList);
  if(MacroList) LoginDataPointer->PasswordMacro = MacroList->FirstChildElement(TElement::Macro);
  if(Protocol != TProtocol::None) 
    memcpy(&InitLoginDataPointer[Protocol], LoginDataPointer, sizeof(TLoginDataPointer));
 //   throw TConfigException("GetFirstLoginData error: no login data");
  return true;
}

bool TConfig::GetNextLoginData(TLoginDataPointer &LoginDataPointer)
{
  TLoginDataPointer l = LoginDataPointer;
  TiXmlElement *e = NULL;
  bool Result = true;
  if(l.Password) e = l.Password->NextSiblingElement();
  if(e)
    l.Password = e;
  else
  {
    if(l.PasswordMacro)
    {
      e = l.PasswordMacro->NextSiblingElement();
      l.Password = l.Password->Parent()->FirstChildElement(TElement::Password);
    }
    if(e)
      l.PasswordMacro = e;
    else
    {
      if(l.Login)
      {
        e = l.Login->NextSiblingElement();
        if(l.PasswordMacro)
          l.PasswordMacro = l.PasswordMacro->Parent()->FirstChildElement(TElement::Password);
      }
      if(e)
        l.Login = e;
      else
      {
        if(l.LoginMacro)
        {
          e = l.LoginMacro->NextSiblingElement();
          l.Login = l.Login->Parent()->FirstChildElement(TElement::Login);
        }
        if(e)
          l.LoginMacro = e;
        else
        {
          if(l.LoginList)
          {
            l.LoginList = l.LoginList->NextSiblingElement(TElement::LoginList);
            if(l.LoginList)
              Result = GetFirstLoginData(&l);
          }
          if(!l.LoginList) Result = false;
        }
      }
    }
  }
  if(!Result)
  {
    l.LoginList = NULL;
    l.LoginMacro = NULL;
    l.Login = NULL;
    l.PasswordMacro = NULL;
    l.Password = NULL;
  }
  LoginDataPointer = l;
  return Result;
}

TiXmlElement *TConfig::GetFirstAddress(TProtocol::Value Protocol)
{
  for(TiXmlElement *Addresses = ConfigElement->FirstChildElement(TElement::AddressList);
    Addresses; Addresses = Addresses->NextSiblingElement())
  {
    const char *p = Addresses->Attribute(TElement::Protocol);
    if(!p) throw TConfigException("xml error: protocol attribute is mandatory");
    if(string(p) == TProtocol::String[Protocol])
      return Addresses->FirstChildElement(TElement::Address);
  }
  return NULL;
}


UINT TConfig::IpToUlong(const string &IpString)
{
  ULONG a = inet_addr(IpString.c_str());
  if(a == INADDR_NONE)
    return 0;
  else
    return ntohl(a);
}

std::string TConfig::UlongToIp(ULONG IpUlong)
{
  union TIp 
  {
    ULONG Ulong;
    unsigned char Bytes[8];
  };
  char s[20];
  TIp ip;
  ip.Ulong = IpUlong;
  sprintf_s(s, "%d.%d.%d.%d", ip.Bytes[3], ip.Bytes[2], ip.Bytes[1], ip.Bytes[0]);
  return string(s);
}

bool TConfig::ExpandDomain(string &Macro, string Domain)
{
  if(Macro.empty()) return true;
  static string DomainToken = "$domain";
  static string Domain2Token = "$domain2";
  if(mismatch(DomainToken.rbegin(), DomainToken.rend(), Macro.rbegin()).first
    == DomainToken.rend())
  {
    Macro.replace(Macro.size() - DomainToken.size(), DomainToken.length(), Domain);
    return true;
  }

  if(mismatch(Domain2Token.rbegin(), Domain2Token.rend(), Macro.rbegin()).first
    == Domain2Token.rend())
  {
    size_t i = Domain.find('.');
    if(i != string::npos) Domain.resize(i);
    Macro.replace(Macro.size() - Domain2Token.size(), Domain2Token.length(), Domain);
    return true;
  }
  return false;
}




//bool TConfig::AddAddresses(TiXmlElement *Addresses)
  //{
  //  TProtocol::Value Proto = (Addresses->Attribute(TElement::Protocol) == TProtocol::String[TProtocol::FTP]) ?
  //    TProtocol::FTP : TProtocol::SSH;
  //  TiXmlElement *Logins = Addresses->FirstChildElement(TElement::LoginList);
  //  if(!Logins) return false;
  //  for(TiXmlElement *AddressElement = Addresses->FirstChildElement(TElement::Address); AddressElement;
  //    AddressElement = AddressElement->NextSiblingElement())
  //  {
  //    //if(!AddressElement->FirstChild() || !AddressElement->FirstChild()->ToText()) continue;
  //    //TiXmlText *AddressText = AddressElement->FirstChild()->ToText();
  //    //ULONG StartIp = 0;
  //    //if(AddressElement->Attribute(TElement::StartIp))
  //    //  StartIp = IpToUlong(AddressElement->Attribute(TElement::StartIp));
  //    //ULONG EndIp = IpToUlong(AddressElement->Attribute(TElement::EndIp));
  //    //for(ULONG i = StartIp; i <= EndIp; i++)
  //    //{
  //    //  AddressList[Proto].push_back(UlongToIp(i));
  //    //  continue;
  //    //}
  //    string a = AddressElement->FirstChild()->ValueStr();
  //    AddressList[Proto].push_back(a);
  //    continue;
  //  }
  //
  //  for( ; Logins; Logins = Logins->NextSiblingElement(TElement::LoginList))
  //    AddLoginMacros(Logins, (int)Proto);
  //
  //  return true;
  //}
  //
  //bool TConfig::AddLoginMacros(TiXmlElement *Logins, int ProtocolIndex)
  //{
  //  AddLogins(Logins, ProtocolIndex, string(""));
  //  TiXmlElement *Macros = Logins->FirstChildElement(TElement::MacroList);
  //  for(TiXmlElement *MacroElement = Macros->FirstChildElement(TElement::Macro);
  //    MacroElement; MacroElement = MacroElement->NextSiblingElement())
  //  {
  //    if(!MacroElement->FirstChild() || !MacroElement->FirstChild()->ToText()) continue;
  //    string Macro = MacroElement->FirstChild()->ToText()->ValueStr();
  //    AddLogins(Logins, ProtocolIndex, Macro);
  //  }
  //  return true;
  //}
  //
  //bool TConfig::AddLogins(TiXmlElement *Logins, int ProtocolIndex, string Macro)
  //{
  //  for(TiXmlElement *LoginElement = Logins->FirstChildElement(TElement::Login); LoginElement;
  //    LoginElement = LoginElement->NextSiblingElement())
  //  {
  //    TiXmlText *LoginText = LoginElement->FirstChild()->ToText();
  //    if(!LoginText) continue;
  //    if(Macro == "")
  //    {
  //      //LoginList[ProtocolIndex].push_back(LoginText->ValueStr());
  //      continue;
  //    }
  //    size_t i = Macro.find('@');
  //    if(i == string::npos) continue;
  //    Macro.replace(i, 1, LoginText->ValueStr());
  //    //LoginList[ProtocolIndex].push_back(Macro);
  //
  //    //ExpandDomain(LoginText, )
  //    //LoginList[ProtocolIndex].push_back(LoginText->ValueStr());
  //  }
  //  return true;
  //}

//void TConfig::SkipComments(TiXmlNode *&Node)
//{
//  while(Node && Node->Type() == TiXmlNode::TINYXML_COMMENT)
//    Node = Node->NextSibling();
//}

//bool TConfig::GetRange(const string &Addr, ULONG &StartIp, ULONG &EndIp)
//{
//  const char *RangeDelim = " - ";
//  size_t RangeDelimPos = Addr.find(RangeDelim);
//  if(RangeDelimPos == string::npos)
//    return false;
//  else
//  {
//    StartIp = ntohl(inet_addr(Addr.substr(0, RangeDelimPos).c_str()));
//    EndIp = ntohl(inet_addr(Addr.substr(RangeDelimPos + sizeof(RangeDelim) - 1).c_str()));
//  }
//  return true;
//}



//TStrings
//TXmlStrings::TXmlStrings(TiXmlText *AXmlText)
//{
//  XmlText = AXmlText;
//  XmlText->SetCDATA(true);
//  const char *c = XmlText->Value();
//  if(*c == '\n') c++;
//  while(*c == ' ') c++;
//  XmlText->SetUserData((void *)c);
//}
//
//bool TXmlStrings::First()
//{
//  return false;
//}
//
//bool TXmlStrings::Next()
//{
//  char *c = (char *)XmlText->GetUserData();
//  //if(*c == '\n') c++;
//  while(*c != '\n' && *c != 0) c++;
//  while(*c != ' ' && *c != 0) c++;
//  XmlText->SetUserData((void *)c);
//  return *c == 0;
//}
//
//bool TXmlStrings::Eol()
//{
//  return *((char *)XmlText->GetUserData()) == 0;
//}
//
//std::string TXmlStrings::CurrentString()
//{
//  char *i = (char *)XmlText->GetUserData();
//  if(*i == '\n') i++;
//  while(*i == ' ') i++;
//  char *j = i;
//  while(*j != '\n' && *j != 0) j++;
//  string s(i, j - i);
//  return s;
//}

TConfigException::TConfigException(string Text)
  : runtime_error("Config error:"), Text(Text)
{
}

const char* TConfigException::what() const throw()
{
    return Text.c_str();
}
