// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _WIN32_WINNT 0x0500
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <stdio.h>
#include <tchar.h>
#include <Windows.h>
#include <iostream>
#include <conio.h>
#include <memory>
//#define BOOST_ALL_NO_LIB
//#include <boost/move/unique_ptr.hpp>
//#include <boost/pool/pool.hpp>
//#include <boost/thread.hpp>
//#include <boost/bind.hpp>
//#include <boost/shared_ptr.hpp>
//#include <boost/asio.hpp>
//#include <boost/noncopyable.hpp>


// TODO: reference additional headers your program requires here
