#include "stdafx.h"

//namespace NT
//{
//#include "ntdll.h"
////typedef LONG NTSTATUS;
////#include "ntos.h"
//}

using namespace std;
//using namespace boost::thread_group::;
//
//class TThreadPool;
//
//// worker thread objects
//class TWorker 
//{
//public:
//  TWorker(TThreadPool &s) : pool(s) { }
//  void operator()();
//private:
//  TThreadPool &pool;
//};
//
//// the actual thread pool
//class TThreadPool 
//{
//private:
//  // need to keep track of threads so we can join them
//  //std::vector< std::unique_ptr<boost::thread> > workers;
//  //std::vector< boost::shared_ptr<boost::thread> > workers;
//  boost::thread_group workers;
//  // the io_service we are wrapping
//  boost::asio::io_service Service;
//  boost::asio::io_service::work IsWorking;
//  friend class TWorker;
//public:
//  TThreadPool(size_t);
//  ~TThreadPool();
//  template<class F> void enqueue(F f);
//  void DoWork(int i);
//};
//
//// all the workers do is execute the io_service
//void TWorker::operator()() 
//{
//  pool.Service.run();
//}
//
//// the constructor just launches some amount of workers
//TThreadPool::TThreadPool(size_t threads) : IsWorking(Service)
//{
//  for(size_t i = 0; i < threads; ++i)
//    workers.create_thread<TWorker>(TWorker(*this));
//    //workers.push_back
//    //(
//    //  //std::unique_ptr<boost::thread>(
//    //  //boost::scoped_ptr //optimize
//    //  boost::shared_ptr<boost::thread>
//    //  (
//    //    new boost::thread(TWorker(*this))
//    //  )
//    //);
//}
//
//// add new work item to the pool
//template<class F>
//void TThreadPool::enqueue(F f)
//{
//  Service.post(f);
//}
//
//// the destructor joins all threads
//TThreadPool::~TThreadPool()
//{
//  Service.stop();
//  //for(size_t i = 0; i<workers.size(); ++i)
//  //  workers[i]->join();
//  workers.join_all();
//}
//
//void TThreadPool::DoWork(int i)
//{
//  std::cout << "hello " << i << std::endl;
//  boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
//  std::cout << "world " << i << std::endl;
//}
//
//void main()
//{
//  // create a thread pool of 4 worker threads
//  TThreadPool pool(4);
//  // queue a bunch of "work items"
//  for(int i = 0; i < 8; i++)
//    pool.enqueue(
//    boost::BOOST_BIND(&TThreadPool::DoWork, &pool, i)
//    );
//  getch();
//}




//typedef std::size_t (*PWorkerFunc)();
//c++11
//struct thread_pool
//{
//  typedef boost::movelib::unique_ptr<boost::asio::io_service::work> asio_worker;
//  thread_pool(int threads) ::service(), service_worker(new asio_worker::element_type(service)) {
//    for (int i = 0; i < threads; ++i)
//    {
//      //auto worker = [this] { return service.run(); };
//      PWorkerFunc worker = (PWorkerFunc&)WorkerFunc;
//      grp.add_thread(new boost::thread(worker));
//    }
//  }
//
//  template<class F>
//  void enqueue(F f) {
//    service.post(f);
//  }
//
//  ~thread_pool() {
//    service_worker.reset();
//    grp.join_all();
//    service.stop();
//  }
//
//  static std::size_t WorkerFunc()
//  {
//    return service.run();
//  }
//
//private:
//  boost::asio::io_service service;
//  asio_worker service_worker;
//  boost::thread_group grp;
//};





const int ApcCount = 2;
const int ThreadCount = 4;
HANDLE Threads[ThreadCount];

DWORD CALLBACK ThreadFunc(LPVOID p)
{
  HANDLE hEvent = (HANDLE)p;
  SetEvent(hEvent);
  int i = 0;
  SleepEx(INFINITE, true);  //hEvent will be set
  return 0;
}

VOID CALLBACK APCProc(ULONG_PTR dwParam)
{
  cout << "APC Proc #" << dwParam;
  cout << " threadid :" << GetCurrentThreadId() << endl;
}

int TestAPC()
{
  HANDLE Event = CreateEvent(0, false, false, NULL);
  DWORD trd_id = 0;
  for(int i = 0; i < ThreadCount; i++)
  {
    Threads[i] = CreateThread(0, 0, ThreadFunc, Event, 0, &trd_id);
    cout << "Thread id is 0x" << hex << trd_id << endl;
    WaitForSingleObject(Event, INFINITE);  //return on hThread become alertable
    ResetEvent(Event);
  }

  for(int i = 0; i < ApcCount; i++)
    QueueUserAPC(APCProc, Threads[i], i);
  WaitForMultipleObjects(ThreadCount, Threads, true, 1000);
  for(int i = 0; i < ThreadCount; i++)
    CloseHandle(Threads[i]);
  
  _getch();

  return 0;

}

void main()
{
  TestAPC();
}