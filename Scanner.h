//TScanner - ������� ����� ������. ��������� ������ � ����� �������
#pragma once
#include "Config.h"
#include "Server.h"
#include "ThreadPool.h"
#include "Connector.h"

typedef void (CALLBACK *TOutputDataBlockEvent)(list<TConnection *> &Results, void *CallbackData);
typedef void (CALLBACK *TAddressDoneEvent)(string Address, void *CallbackData);
typedef void (CALLBACK *TServerDoneEvent)(TServer *Server, void *CallbackData);

extern TConfig *Config;

class TServerList 
{
public:
  list<TServer *> Items;
  bool Add(TAddressPointer &AddressPointer, TProtocol::Value Protocol);
  void Free();
};

class TConnections
{
public:
  list<TConnection *> Items;
  void Free();
};

class TScannnerException : public std::runtime_error
{
private:
  string Text;
public:
  TScannnerException(string Text);
  const char* what() const throw();
};

class TScanner
{
private:
  TServerList ServerList;
  TConnections Results;
  CRITICAL_SECTION ServerListGuard;//synchronize ServerConnectComplete and FeedLogins
  TThreadPool *ThreadPool;
  HANDLE FeedThread;
  static DWORD ScanStartMark;
  static DWORD WINAPI FeedLoginsCallback(CONST LPVOID Param);
  void FeedTasks();
public:
  static const bool UseAsyncFTPConnection;
  TConfig *Config;
  TConnector *Connector;
  bool StopScan;
  void *CallbackData;
  TConnectionRequestCompleteEvent OnRequestComplete;
  TOutputDataBlockEvent OnOutputDataBlock;
  TServerDoneEvent OnServerDone;
  TAddressDoneEvent OnAddressDone;
  TScanner(void *ACallbackData);
  ~TScanner();
  void ServerConnectComplete(TConnection *Connection);
  static DWORD ScanTime();
  bool Scan();
  bool InitFromConfig(TConfig &Config);
  bool WaitForScanComplete(DWORD Timeout);
  //void StopScan();
};
