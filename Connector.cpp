#include "stdafx.h"
#include "Connector.h"
#include <tchar.h>
#include <stdlib.h>
#include "Scanner.h"
#include "ThreadPool.h"
#include "Server.h"

//#include "ftplib/src/ftplib.h"

#define strncasecmp _strnicmp
#define strcasecmp _stricmp

const int ConnectTimeout = 30;

TConnector *TConnection::Connector = NULL;

//TConnector implementation
TConnector::TConnector(void *CallbackData)//TThreadPool *AThreadPool)
{
  //ThreadPool = AThreadPool;
  this->CallbackData = CallbackData;
  TConnection::Connector = this;
  // ftp init
  DWORD flags = NULL;
  if(TScanner::UseAsyncFTPConnection) flags = INTERNET_FLAG_ASYNC;
  InternetHandle = InternetOpen(NULL, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, flags);
  if(InternetHandle == NULL) throw std::runtime_error("InternetOpen error");
  if(TScanner::UseAsyncFTPConnection)
  {
    INTERNET_STATUS_CALLBACK p = InternetSetStatusCallback(InternetHandle, (INTERNET_STATUS_CALLBACK)FTPAsyncCallback);
    if (p == INTERNET_INVALID_STATUS_CALLBACK)
      throw std::runtime_error("InternetSetStatusCallback error");
  }
  // ssh init
  int rc, err;
  WSADATA wsadata;
  err = WSAStartup(MAKEWORD(2,0), &wsadata);
  if(err != 0) throw std::runtime_error(string("WSAStartup failed with error: %d\n"));
  rc = libssh2_init(0);
  if(rc != 0) throw std::runtime_error("libssh2 initialization failed (%d)\n");
//  InitializeCriticalSection(&SshHandshakeGuard);
}

TConnector::~TConnector(void)
{
  InternetCloseHandle(InternetHandle);
  libssh2_exit();
//  DeleteCriticalSection(&SshHandshakeGuard);
}

void CALLBACK TConnector::FTPAsyncCallback(
  __in HINTERNET hInternet,
  __in DWORD_PTR dwContext,
  __in DWORD dwInternetStatus,
  __in_bcount(dwStatusInformationLength) LPVOID lpvStatusInformation,
  __in DWORD dwStatusInformationLength)
{
  TConnection *Connection = (TConnection *)dwContext;
  TConnector *Connector = Connection->Connector;
  if(!dwContext) throw runtime_error("context is NULL");
  switch(dwInternetStatus)
  {
  case INTERNET_STATUS_HANDLE_CREATED:
    if(Connection->Server->State == TServerState::NotChecked) 
      Connection->Server->State = TServerState::Unreachable;
    return;
  case INTERNET_STATUS_CONNECTING_TO_SERVER:
    if(Connection->Server->State == TServerState::Unreachable)
      Connection->Server->State = TServerState::Available;
    return;
  }
  if(dwInternetStatus != INTERNET_STATUS_REQUEST_COMPLETE) return;
  INTERNET_ASYNC_RESULT* pResult = static_cast<INTERNET_ASYNC_RESULT*>(lpvStatusInformation);
  if(Connection->Server->State != TServerState::Unreachable)
    Connection->Result = (pResult->dwResult != 0);
  if(pResult->dwResult != 0) InternetCloseHandle((HINTERNET)pResult->dwResult);
  //Connection->Connector->ThreadPool->TaskComplete(Connection);
  if(Connector->OnConnectionComplete)
    Connector->OnConnectionComplete(Connection, Connector->CallbackData);
}


ULONG GetIP(const string Address)
{
  char c = Address[Address.length() - 1];
  if(c >= '0' && c <= '9')
    return inet_addr(Address.c_str());
  else
  {
    HOSTENT *h = gethostbyname(Address.c_str()); 
    if(!h) return 0;
    return ((in_addr*)h->h_addr_list[0])->s_addr;
  }
  return 0;
}

//IOCP FTP implementation based on WinApi
void TConnector::ConnectFTPAsync(TConnection *Connection)
{
  if(Connection->Server->Port == INTERNET_INVALID_PORT_NUMBER)
    Connection->Server->Port = INTERNET_DEFAULT_FTP_PORT;
  InternetConnect(InternetHandle, Connection->Server->Address.c_str(), Connection->Server->Port,
    Connection->Login().c_str(), Connection->Password().c_str(),
    INTERNET_SERVICE_FTP, INTERNET_FLAG_ASYNC, (DWORD_PTR)Connection);
}

void TConnector::ConnectFTPBlocking(TConnection *Connection)
{
  if(Connection->Server->Port == INTERNET_INVALID_PORT_NUMBER)
    Connection->Server->Port = INTERNET_DEFAULT_FTP_PORT;
  HINTERNET h = InternetConnect(InternetHandle, Connection->Server->Address.c_str(), Connection->Server->Port,
    Connection->Login().c_str(), Connection->Password().c_str(),
    INTERNET_SERVICE_FTP, NULL, (DWORD_PTR)Connection);
  if(h) 
  {
    InternetCloseHandle(h);
    Connection->Result = true;
  }
  else
  {
    DWORD e = GetLastError();
    if(e = ERROR_INTERNET_NAME_NOT_RESOLVED) 
      Connection->Server->State = TServerState::Unreachable;
  }
}

class TScopedSocket
{
public:
  int Handle;
  void Free() {closesocket(Handle); Handle = NULL;}
  ~TScopedSocket() {if(Handle) Free();}
};

class TScopedSocketState
{
public:
  int Handle;
  TScopedSocketState(int Handle)
  {
    ULONG NonBlk = 1;
    this->Handle = Handle;
    ioctlsocket(Handle, FIONBIO, &NonBlk);
  }
  ~TScopedSocketState()
  {
    ULONG Blk = 0;
    ioctlsocket(Handle, FIONBIO, &Blk);
  }
};

bool ConnectSocket(SOCKET Socket, sockaddr_in AddrIn, UINT TimeoutSec)
{
  TScopedSocketState SocketState(Socket);
  int Ret = connect(Socket, (const sockaddr*)&AddrIn, sizeof(AddrIn));
  if(Ret != SOCKET_ERROR) return true;
  if (WSAGetLastError() != WSAEWOULDBLOCK) return false;
 
  fd_set         Write, Err;
  TIMEVAL      Timeout;
  FD_ZERO(&Write);
  FD_ZERO(&Err);
  FD_SET(Socket, &Write);
  FD_SET(Socket, &Err);

  Timeout.tv_sec  = TimeoutSec;
  Timeout.tv_usec = 0; // your timeout

  Ret = select(0, NULL, &Write, &Err, &Timeout);

  if(Ret == 0) return false; //timeout
  if (FD_ISSET(Socket, &Write)) return true;
  if (FD_ISSET(Socket, &Err)) return false;
  return false;
}

bool FtpWriteTest(int CommandSocket) 
{
  string Command = "PASV\r\n";
  if(send(CommandSocket, Command.c_str(), Command.length(), 0) != Command.length())
    return false;
  char buff[128];
  if(recv(CommandSocket, buff, sizeof(buff), 0) == -1)
    return false;
  char *o = strchr(buff, '(');
  if(!o) return false;
  o++;
  char *c = strchr(o, ')');
  if(!c) return false;
  *c = 0;
  int p[2], a[4];
  sscanf_s(o, "%d,%d,%d,%d,%d,%d", &a[0], &a[1], &a[2], &a[3], &p[0], &p[1]);
  sockaddr_in AddrIn;
  AddrIn.sin_addr.S_un.S_un_b.s_b1 = a[0];
  AddrIn.sin_addr.S_un.S_un_b.s_b2 = a[1];
  AddrIn.sin_addr.S_un.S_un_b.s_b3 = a[2];
  AddrIn.sin_addr.S_un.S_un_b.s_b4 = a[3];

  int port = p[0] * 256 + p[1];

  TScopedSocket DataSocket;
  DataSocket.Handle = socket(AF_INET, SOCK_STREAM, 0);
  AddrIn.sin_family = AF_INET;
  AddrIn.sin_port = htons(port);
  if(!ConnectSocket(DataSocket.Handle, AddrIn, ConnectTimeout)) return false;

  Command = "STOR test.html\r\n";
  if(send(CommandSocket, Command.c_str(), Command.length(), 0) != Command.length())
    return false;
  char Buf[513] = {0};
  if(recv(CommandSocket, Buf, sizeof(Buf), 0) == -1)
    return false;
  if(strncmp(Buf, "125", 3) != 0 && strncmp(Buf, "150", 3) !=0) return false;

  string TestHtml = "<header></header><body></body>";
  if(send(DataSocket.Handle, TestHtml.c_str(), TestHtml.length(), 0) != TestHtml.length())
    return false;
  DataSocket.Free();

  if(recv(CommandSocket, Buf, sizeof(Buf), 0) == -1)
    return false;
  if(strncmp(Buf, "226", 3) != 0) return false;

  return true;
}

void TConnector::ConnectFTPBlockingSock(TConnection *Connection)
{
  ULONG Ip = GetIP(Connection->Server->Address);
  string l = Connection->Login();
  vector<char> v(l.begin(), l.end());
  if(!Ip)
  {
    Connection->Server->State = TServerState::Unreachable;
    return;
  }
  if(Connection->Server->Port == INTERNET_INVALID_PORT_NUMBER)
    Connection->Server->Port = INTERNET_DEFAULT_FTP_PORT;
  sockaddr_in AddrIn;

  AddrIn.sin_family = AF_INET;
  AddrIn.sin_addr.s_addr = Ip;
  AddrIn.sin_port = htons(Connection->Server->Port);
  TScopedSocket Socket;
  Socket.Handle = socket(AF_INET, SOCK_STREAM, 0);

  if(!ConnectSocket(Socket.Handle, AddrIn, ConnectTimeout))
  {
    Connection->Server->State = TServerState::Unreachable;
    return;
  }
  char Buf[513] = {0};
  recv(Socket.Handle, Buf, sizeof(Buf) - 1, 0);
  if(strncmp(Buf, "220", 3) != 0) return;
  string Command = "USER " + Connection->Login() + "\r\n";
  send(Socket.Handle, Command.c_str(), Command.length(), 0);
  ZeroMemory(Buf, sizeof(Buf) - 1);
  int Len = recv(Socket.Handle, Buf, sizeof(Buf) - 1, 0);
  if(strncmp(Buf, "331", 3) != 0) return;  
  ZeroMemory(Buf, sizeof(Buf) - 1);
  Command = "PASS " + Connection->Password() + "\r\n";
  send(Socket.Handle, Command.c_str(), Command.length(), 0);
  ZeroMemory(Buf, sizeof(Buf) - 1);
  recv(Socket.Handle, Buf, sizeof(Buf), 0);
  if(strncmp(Buf, "230", 3) != 0) return;

  Connection->Result = FtpWriteTest(Socket.Handle);
  return;
}

static bool SocketSelect(int Socket, LIBSSH2_SESSION *session, int Timeout)
{
  bool read = false;
  if( session != NULL )
  {
    // now make sure we wait in the correct direction
    int direction = libssh2_session_block_directions( session );
    if( direction & LIBSSH2_SESSION_BLOCK_INBOUND )
      read = true;
    else if( direction & LIBSSH2_SESSION_BLOCK_OUTBOUND )
      read = false;
  }

  fd_set fds;
  FD_ZERO( &fds );
  FD_SET( Socket, &fds );

  struct timeval timeout;
  timeout.tv_sec = Timeout;
  timeout.tv_usec = 0;

  int status = read ? select(Socket + 1, &fds, NULL, NULL, &timeout) :
    select(Socket + 1, NULL, &fds, NULL, &timeout);
  if( status == -1 )
    return false;
  if( FD_ISSET(Socket, &fds) == 0 )         // Timeout
    return false;
  return true;
}

class TScopedSession
{
public:
  int Socket;
  LIBSSH2_SESSION *Handle;
  TScopedSession(int &Socket) : Socket(Socket) {}
  ~TScopedSession()
  {
    if(!Handle) return;

    while(libssh2_session_disconnect(Handle, "") == LIBSSH2_ERROR_EAGAIN)
    {
      if(!SocketSelect(Socket, NULL, ConnectTimeout * 3))
        break;
    }
    libssh2_session_free(Handle);
  }
};

//class TScopedCriticalSection
//{
//public:
//  CRITICAL_SECTION *CriticalSection;
//  TScopedCriticalSection(CRITICAL_SECTION &CriticalSection)
//    : CriticalSection(CriticalSection)
//  { }
//  ~TScopedCriticalSection()
//  {
//    LeaveCriticalSection(CriticalSection)
//  }
//};

//blocking SSH connect implementation based on libssh2
void TConnector::ConnectSSHBlocking(TConnection *Connection)
{
  ULONG Addr = GetIP(Connection->Server->Address.c_str());
  if(!Addr)
  {
    Connection->Server->State = TServerState::Unreachable;
    return;
  }
  TScopedSocket Socket;
  int auth_pw = 0;
  struct sockaddr_in AddrIn;
  char *userauthlist;
  Socket.Handle = socket(AF_INET, SOCK_STREAM, 0);
  if(Socket.Handle == INVALID_SOCKET)
    throw std::runtime_error("Cannot create Socket");
  if(Connection->Server->Port == INTERNET_INVALID_PORT_NUMBER)
    Connection->Server->Port = 22;
  AddrIn.sin_family = AF_INET;
  AddrIn.sin_port = htons(Connection->Server->Port);
  AddrIn.sin_addr.s_addr = Addr;
  if(!ConnectSocket(Socket.Handle, AddrIn, ConnectTimeout))
  {
    Connection->Server->State = TServerState::Unreachable;
    return;
  }
  TScopedSession Session(Socket.Handle);

  Session.Handle = libssh2_session_init();
  const int NoBlock = 0;
  libssh2_session_set_blocking(Session.Handle, NoBlock);
  //libssh2_session_set_timeout(Session.Handle, ConnectTimeout * 2 * 1000);
  if(!Session.Handle) return;
  //if(libssh2_session_handshake(Session.Handle, Socket.Handle) != 0) return;
  int rc;
  
//  EnterCriticalSection(&SshHandshakeGuard);
//  try {
    while((rc = libssh2_session_handshake(Session.Handle, Socket.Handle)) == LIBSSH2_ERROR_EAGAIN )
    {
      //LeaveCriticalSection(&SshHandshakeGuard);
      if(!SocketSelect(Socket.Handle, NULL, ConnectTimeout * 2))
        break;
      //EnterCriticalSection(&SshHandshakeGuard);
    }
  //} catch(...)
  //{
  //  LeaveCriticalSection(&SshHandshakeGuard);
  //}
  if(rc != 0) return;

  //userauthlist = libssh2_userauth_list(Session.Handle, Connection->Login().c_str(), (UINT)Connection->Login().size());
  string Login = Connection->Login();
  while((userauthlist = libssh2_userauth_list(Session.Handle, Login.c_str(), (UINT)Login.size())) == NULL &&
    libssh2_session_last_error(Session.Handle, NULL, NULL, 0) == LIBSSH2_ERROR_EAGAIN)
  {
    if(!SocketSelect(Socket.Handle, NULL, ConnectTimeout * 3))
      break;
  }
  if(!userauthlist) return;
  if(strstr(userauthlist, "password") == NULL) return;
  //Connection->Result = (libssh2_userauth_password(Session.Handle,
  //  Connection->Login().c_str(), Connection->Password().c_str()) > 0);
  string Pass = Connection->Password();
  while((rc = libssh2_userauth_password(Session.Handle, Login.c_str(), Pass.c_str())) == LIBSSH2_ERROR_EAGAIN)
  {
    if(!SocketSelect(Socket.Handle, NULL, ConnectTimeout * 3))
      break;
  }
  Connection->Result = (rc >= 0);
  //libssh2_session_disconnect(Session.Handle, "");
}

void TConnector::Connect(TConnection *Connection)
{
  Connection->Result = false;
  switch(Connection->Server->Protocol)
  {
  case TProtocol::FTP:
    if(TScanner::UseAsyncFTPConnection)
      ConnectFTPAsync(Connection);
    else
    {
      try {
        ConnectFTPBlockingSock(Connection);
      } catch (...)
      {}
    }
    break;
  case TProtocol::SSH:
    try {
      ConnectSSHBlocking(Connection);
    } catch(...)//const runtime_error& e)
    {}
    break;
  }
  if(OnConnectionComplete)
    OnConnectionComplete(Connection, CallbackData);
}

//TConnection implementation
TConnection::TConnection(TServer *AServer, string ALogin, string APassword)
{
  Server = AServer;
  FLogin = ALogin;
  FPassword = APassword;
}

std::string TConnection::Login()
{
  //return *Server->Logins[LoginIndex];
  return FLogin;
}

std::string TConnection::Password()
{
  //return *Server->Passwords[LoginIndex];
  return FPassword;
}



//TODO: IOCP
//HANDLE FTPAsyncEvent = 0;
//GUID ConnectExGUID = WSAID_CONNECTEX;
//void CALLBACK CompletionROUTINE(
//  DWORD dwError,
//  DWORD cbTransferred,
//  LPWSAOVERLAPPED Overlapped,
//  DWORD dwFlags
//  )
//{
//}
//LPWSAOVERLAPPED Overlapped = new WSAOVERLAPPED();//initialized by 0
//DWORD dwT = 0;
//int nRet = WSAIoctl(sock, SIO_GET_EXTENSION_FUNCTION_POINTER, &ConnectExGUID, sizeof(GUID),
//  &ConnectEx, sizeof(ConnectEx), &dwT, Overlapped, &CompletionROUTINE);
//if(nRet == SOCKET_ERROR) throw std::runtime_error("WSAIoctl error");
//if(!ConnectEx) throw std::runtime_error("Cannot get pointer to ConnectEx");
//{
//  struct sockaddr_in addr;
//  ZeroMemory(&addr, sizeof(addr));
//  addr.sin_family = AF_INET;
//  addr.sin_addr.s_addr = INADDR_ANY;
//  addr.sin_port = 0;
//  int rc = bind(sock, (SOCKADDR*)&addr, sizeof(addr));
//  if(rc != 0) throw std::runtime_error("ind failed ");//WSAGetLastError());
//}
//int err = setsockopt(sock, SOL_SOCKET, SO_UPDATE_CONNECT_CONTEXT, NULL, 0);
//BOOL ok = ConnectEx(sock, (sockaddr *)&sin, sizeof(sin), NULL, 0, NULL, Overlapped);
//if(!ok)
//  if(WSAGetLastError() == ERROR_IO_PENDING)
//    printf("ConnectEx pending\n");

