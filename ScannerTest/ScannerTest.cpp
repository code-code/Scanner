// ScannerTest.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "..\Scanner.h"

CRITICAL_SECTION OutBlockGuard;

void CALLBACK ConnectionRequestComplete(TConnection *Connection, void *CallackData)
{
  EnterCriticalSection(&OutBlockGuard);
  cout << "Address:  " << Connection->Server->Address << "\r\n";
  cout << "Protocol: " << TProtocol::String[Connection->Server->Protocol] << "\r\n";
  cout << "Login:    " << Connection->Login() << "\r\n";
  cout << "Password: " << Connection->Password() << "\r\n";
  cout << "Result:   " << Connection->Result << "\r\n\r\n";
  LeaveCriticalSection(&OutBlockGuard);
  //delete Connection;
}

void CALLBACK OutputDataBlock(list<TConnection *> &Results, void *CallbackContext)
{
  EnterCriticalSection(&OutBlockGuard);
  cout << "************ Results block *************\r\n";
  for(list<TConnection *>::iterator i = Results.begin(); i != Results.end(); i++)
  {
    TConnection *Connection = *i;
    cout << "Address:  " << Connection->Server->Address << "\r\n";
    cout << "Login:    " << Connection->Login() << "\r\n";
    cout << "Password: " << Connection->Password() << "\r\n";
    cout << "Result:   " << Connection->Result << "\r\n\r\n";
  }
  LeaveCriticalSection(&OutBlockGuard);
}

int main()
{
  InitializeCriticalSection(&OutBlockGuard);
  TScanner s(NULL);
  s.OnRequestComplete = ConnectionRequestComplete;
  s.OnOutputDataBlock = OutputDataBlock;
  s.Config->LoadFromFile("..\\config.xml");
  s.Scan();
  if(!s.WaitForScanComplete(INFINITE))
    cout << "\r\nTimeout!";
  cout << "\r\nPress any key to exit";
  _getch();
  return 0;
}

